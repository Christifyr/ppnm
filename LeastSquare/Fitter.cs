using System;
using static System.Math;
using static System.Console;
using static QRMaker;
using System.Collections.Generic;
public struct Fitter{
	public static Func<double[],double[],double[],List<Func<double,double>>,Tuple<vector,matrix>> leastSquareFit = (x,y,eps,fs) => {
		int m = fs.Count;
		int n=x.Length;
		matrix A = new matrix(n,m);
		vector b = new vector(n);
		for(int i=0;i<n;i++){
			for(int k=0;k<m;k++){
				A[i,k] = fs[k](x[i])/eps[i];
			}
			b[i]=y[i]/eps[i];
		}
		matrix R = new matrix(m,m);
		qr_gs_decomp(A,R);
		vector c =qr_gs_solve(A,R,b);
		matrix Rm = new matrix(m,m);
		qr_gs_decomp(R,Rm);
		matrix Rin = new matrix(m,m);
		qr_gs_inverse(R,Rm,Rin);
		return new Tuple<vector,matrix>(c,Rin*Rin.T);
	};
}
