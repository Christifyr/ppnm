using System;
using static System.Math;
using static Fitter;
using static System.Console;
using System.Collections.Generic;
class main{
	public static void Main(string[] args){
		List<Func<double,double>> fs = new List<Func<double,double>>();
		fs.Add((x) => x);
		fs.Add((x) => 1);
		var out1 = new System.IO.StreamWriter(args[0],false);
		var out2 = new System.IO.StreamWriter(args[1],false);
		double[] xs = new double[]{1,2,3,4,6,9,10,13}, ys= new double[]{117,100,88,72,53,29.5,25.2,15.2,11.1}, eps = new double[xs.Length];
		for(int i =0; i<xs.Length;i++){
			out1.WriteLine($"{xs[i]} {ys[i]} {ys[i]/20}");
			eps[i] = 1.0/20;
			ys[i]=Log(ys[i]);
		}
		out1.Close();
		Tuple<vector,matrix> t = leastSquareFit(xs,ys,eps,fs);
		vector c = t.Item1;
		matrix dc = t.Item2;
		double dx = 1.0/16;
		for(double x = 1; x<22; x+=dx){
			out2.WriteLine($"{x}	{Exp(c[1])*Exp(c[0]*x)}	{Exp(c[1]+Sqrt(dc[1,1]))*Exp((c[0]+Sqrt(dc[0,0]))*x)}	{Exp(c[1]-Sqrt(dc[1,1]))*Exp((c[0]-Sqrt(dc[0,0]))*x)}");
		}
		out2.Close();
		WriteLine($"The half life from the fit is: {-Log(2)/c[0]} days");
		WriteLine($"The reference says that it is about 3.63 days so it is a bit off");
		WriteLine($"The uncertanty on the half life is: {Log(2)/Pow(c[0],2)*(Sqrt(dc[0,0]))}");
		WriteLine($"So it is still not within the uncertanty");
		}
}
