using System;
using static System.Math;
using static System.Console;
using System.Collections.Generic;
class main{
	public static void Main(){
		Func<double,vector,vector> myfun = (x,y) => new vector(y[0]*(1-y[0]));
		double a = 0;
		double b = 3;
		vector ya = new vector(0.5);
		List<double> xs = new List<double>();
		List<vector> ys = new List<vector>();
		vector yb=ode.rk23(myfun,a,ya,b,xs,ys);
		for(int i=0; i<xs.Count; i++){
			WriteLine($"{xs[i]} {ys[i][0]}");	
		}
	}
}
