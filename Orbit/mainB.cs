using System;
using static System.Math;
using static System.Console;
using System.Collections.Generic;
class main{
	public static Func<double,vector,vector> myfun(double eps){
		return (x,y) => new vector(y[1],1-y[0]+eps*y[0]*y[0]);
	}
	public static void Main(string[] args){
		double a = 0.0;
		double b = 3*PI;
		var output1 = new System.IO.StreamWriter(args[0],append:true);
		var output2 = new System.IO.StreamWriter(args[1],append:true);
		var output3 = new System.IO.StreamWriter(args[2],append:true);
		vector ya1 = new vector(1,1e-7);
		List<double> xs1 = new List<double>();
		List<vector> ys1 = new List<vector>();
		vector yb1 = ode.rk23(myfun(0),a,ya1,b,xs1,ys1,1e-12,1e-12);
		for(int i=0;i<xs1.Count;i++){
			output1.WriteLine($"{xs1[i]}	{ys1[i][0]}	{ys1[i][1]}");
		}
                vector ya2 = new vector(1,-0.5);
                List<double> xs2 = new List<double>();
                List<vector> ys2 = new List<vector>();
                vector yb2 = ode.rk23(myfun(0),a,ya2,b,xs2,ys2);
                for(int i=0;i<xs2.Count;i++){
	                output2.WriteLine($"{xs2[i]}     {ys2[i][0]}      {ys2[i][1]}");
                }
                List<double> xs3 = new List<double>();
                List<vector> ys3 = new List<vector>();
                vector yb3 = ode.rk23(myfun(0.01),a,ya2,b,xs3,ys3);
                for(int i=0;i<xs3.Count;i++){
                        output3.WriteLine($"{xs3[i]}     {ys3[i][0]}      {ys3[i][1]}");
                }
	}
}
