using System;
using static System.Math;
using static System.Console;
class main{
	public static void Main(){
		var rnd = new Random(1);
		var timer = System.Diagnostics.Stopwatch.StartNew();
		int n = 4;
		matrix A = new matrix(n,n);
		for(int i = 0; i<n;i++){
			for(int j=0; j<n;j++){
				A[i,j] = rnd.NextDouble()+2;
			}
		}
		matrix V = new matrix(n,n);
		matrix AA = A.copy();
		matrix VV = V.copy();
		WriteLine($"The number of operations and time it takes to find eigenval for a {n}X{n} matrix is:");
		var start = timer.Elapsed;
		WriteLine($"number of sweep: {JacobiEigen.cyclicDecomp(A,V)} and the time in sekunds: {(timer.Elapsed-start).TotalSeconds}");
		WriteLine($"The amount of operations to do the classic decomp:");
		start = timer.Elapsed;
		WriteLine($"number of sweep: {JacobiEigen.classicDecomp(AA,VV)} and the time in sekunds: {(timer.Elapsed-start).TotalSeconds}");
		WriteLine("And making sure they are the same:");
		A.print("The full run");
		AA.print("The values found should be the same as the one above");
	}
}
