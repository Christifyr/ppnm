using System;
using static System.Math;
using static System.Console;
class main{
	public static void Main(){
		int n = 4;
		matrix A = new matrix(n,n);
		var rnd = new Random();
		matrix V = new matrix(n,n);
		for(int i = 0; i<n;i++){
			for(int j=i;j<n;j++){
				A[i,j] = 2*rnd.NextDouble(); A[j,i]=A[i,j];
			}	
		}
		matrix B = A.copy();
		JacobiEigen.cyclicDecomp(A,V);
		(V.T*B*V).print("V.T*A*V=");
		A.print("Eigenval:");
		WriteLine((V.T*B*V).approx(A));
	}
}
