using System;
using static System.Math;
using static System.Console;
class main{
	public static void Main(){
		var rnd = new Random(1);
		int n = 4;
		matrix A = new matrix(n,n);
		for(int i = 0; i<n;i++){
			for(int j=0; j<n;j++){
				A[i,j] = rnd.NextDouble()+2;
			}
		}
		matrix V = new matrix(n,n);
		matrix AA = A.copy();
		matrix VV = V.copy();
		WriteLine($"The number of operations to find eigenval for a {n}X{n} matrix is:");
		WriteLine(JacobiEigen.cyclicDecomp(A,V));
		vector e = new vector(4);
		vector ee = new vector(4);
		matrix AAA = AA.copy();
		matrix VVV = VV.copy();
		WriteLine($"The amount of operations to find a single eigenval is:");
		WriteLine(JacobiEigen.lowestEigenDecomp(AA,VV,e,1));
		WriteLine("And making sure they are the same:");
		e.print("The eigenval found in a single run");
		A.print("The full run");
		WriteLine("The full operations by doing row by row:");
		WriteLine(JacobiEigen.lowestEigenDecomp(AAA,VVV,ee,4));
		ee.print("The values found should be the same as the two above");
	}
}
