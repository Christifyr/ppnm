using System;
using static System.Math;
public struct JacobiEigen{
	        public static Func<matrix,matrix,int> cyclicDecomp = (A,V) =>{
                vector e = new vector(A.size1);
                int sweep = 0;
                V.set_identity();
                for(int i=0;i<A.size1;i++){ e[i]=A[i,i];}
                int changed =1;
		int n = A.size1;
                while(changed==1){
                changed = 0;
                sweep++;
                for(int p=0;p<A.size1;p++){
                        for(int q=p+1;q<A.size1;q++){
                                double app = e[p];
                                double aqq = e[q];
                                double apq = A[p,q];
                                double phi = 0.5*Atan2(2*apq,aqq-app);
                                double c = Cos(phi), s = Sin(phi);
                                double appm = c*c*app-2*c*s*apq+s*s*aqq;
                                double aqqm = c*c*aqq+2*c*s*apq+s*s*app;
                                if(appm!=app || aqqm!=aqq){
                                        changed = 1;
                                        e[p]=appm;
                                        e[q]=aqqm;
                                        A[p,q] = 0.0;
                                        for(int i = 0; i<p; i++){
                                                double aip = A[i,p];
                                                double aiq = A[i,q];
                                                A[i,p] = c*aip-s*aiq;
                                                A[i,q] = c*aiq+s*aip;
                                        }
                                        for(int i = p+1; i<q; i++){
                                                double api = A[p,i];
                                                double aiq = A[i,q];
                                                A[p,i] = c*api-s*aiq;
                                                A[i,q] = c*aiq+s*api;
                                        }
                                        for(int i = q+1; i<A.size1; i++){
                                                double api = A[p,i];
                                                double aqi = A[q,i];
                                                A[p,i] = c*api-s*aqi;
                                                A[q,i] = c*aqi+s*api;
                                        }
                                        for(int i = 0; i<A.size1; i++){
                                                double vip = V[i,p];
                                                double viq = V[i,q];
                                                V[i,p] = c*vip-s*viq;
                                                V[i,q] = c*viq+s*vip;
                                        }
                                }
                        }
                }}
                A.set_identity();
                for(int i=0;i<A.size1;i++){A[i,i]=e[i];}
                return sweep;
        };

	public static Func<matrix,matrix,int> classicDecomp = (A,V) =>{
		vector e = new vector(A.size1);
		int sweep = 0;
		V.set_identity();
		for(int i=0;i<A.size1;i++){ e[i]=A[i,i];}
		int changed =1;
		int[] indecies= new int[A.size1];
		for(int i=0; i<A.size1;i++){
			double currentBig = 0;
			int indexOfBig =0;
			for(int j=i+1;j<A.size1;j++){
				if(Abs(A[i,j])>currentBig){
					currentBig=Abs(A[i,j]);
					indexOfBig = j;
				}
			}
			indecies[i]=indexOfBig;
		}		
		while(changed==1){
		changed = 0;
		sweep++;
		int row = 0;
		foreach(int index in indecies){
				if(index!=0){
				double app = e[row];
				double aqq = e[index];
				double apq = A[row,index];
				double phi = 0.5*Atan2(2*apq,aqq-app);
				double c = Cos(phi), s = Sin(phi);
				double appm = c*c*app-2*c*s*apq+s*s*aqq;
				double aqqm = c*c*aqq+2*c*s*apq+s*s*app;
				if(appm!=app || aqqm!=aqq){
					changed = 1;
					e[row]=appm;
					e[index]=aqqm;
					A[row,index] = 0.0;
					for(int i = 0; i<row; i++){
						double aip = A[i,row];
						double aiq = A[i,index];
						A[i,row] = c*aip-s*aiq;
						A[i,index] = c*aiq+s*aip;
					}
                                        for(int i = row+1; i<index; i++){
                                                double api = A[row,i];
                                                double aiq = A[i,index];
                                                A[row,i] = c*api-s*aiq;
                                                A[i,index] = c*aiq+s*api;
                                        }
                                        for(int i = index+1; i<A.size1; i++){
                                                double api = A[row,i];
                                                double aqi = A[index,i];
                                                A[row,i] = c*api-s*aqi;
                                                A[index,i] = c*aqi+s*api;
                                        }
                                        for(int i = 0; i<A.size1; i++){
                                                double vip = V[i,row];
                                                double viq = V[i,index];
                                                V[i,row] = c*vip-s*viq;
                                                V[i,index] = c*viq+s*vip;
                                        }
				}}
                                double currentBig = 0;
                                int indexOfBig =0;
                                for(int j=row+1;j<A.size1;j++){
                                	if(Abs(A[row,j])>currentBig){
                                        	currentBig=Abs(A[row,j]);
                                		indexOfBig = j;
                                	}
                                }
                                indecies[row]=indexOfBig;
				row++;
			}
		}
		A.set_identity();
		for(int i=0;i<A.size1;i++){A[i,i]=e[i];}
		return sweep;
	};
	
	public static Func<matrix,matrix,vector,int,int> lowestEigenDecomp = (A,V,e,n) =>{
	        int sweep = 0;
	        V.set_identity();
		for(int i=0;i<A.size1;i++){ e[i]=A[i,i];}
		for(int p = 0; p<n;p++){
			int changed = 1;
			while(changed == 1){
				sweep++;
				changed = 0;
				for(int q=p+1;q<A.size1;q++){
                                double app = e[p];
				double apq = A[p,q];
				double aqq = e[q];
                                double phi = 0.5*Atan2(2*apq,aqq-app);
                                double c = Cos(phi), s = Sin(phi);
                                double appm = c*c*app-2*c*s*apq+s*s*aqq;
                                double aqqm = c*c*aqq+2*c*s*apq+s*s*app;
				if(appm!=app){
                                        changed = 1;
                                        e[p]=appm;
					e[q] = aqqm;
                                        A[p,q] = 0.0;
                                	for(int i = 0; i<p; i++){
                                                double aip = A[i,p];
                                                double aiq = A[i,q];
                                                A[i,p] = c*aip-s*aiq;
                                                A[i,q] = c*aiq+s*aip;
                                        }
                                        for(int i = p+1; i<q; i++){
                                                double api = A[p,i];
                                                double aiq = A[i,q];
                                                A[p,i] = c*api-s*aiq;
                                                A[i,q] = c*aiq+s*api;
                                        }
                                        for(int i = q+1; i<A.size1; i++){
                                                double api = A[p,i];
                                                double aqi = A[q,i];
                                                A[p,i] = c*api-s*aqi;
                                                A[q,i] = c*aqi+s*api;
                                        }
                                        for(int i = 0; i<A.size1; i++){
                                                double vip = V[i,p];
                                                double viq = V[i,q];
                                                V[i,p] = c*vip-s*viq;
                                                V[i,q] = c*viq+s*vip;
                                        }

				}
                  	}
		}}
		return sweep;
	};

	public static Func<matrix,matrix,vector,int,int> highestEigenDecomp = (A,V,e,n) =>{ 
                int sweep = 0; 
                V.set_identity(); 
                for(int i=0;i<A.size1;i++){ e[i]=A[i,i];} 
                for(int p = 0; p<n;p++){ 
                        int changed = 1; 
                        while(changed == 1){ 
                                sweep++; 
                                changed = 0; 
                                for(int q=p+1;q<A.size1;q++){ 
                                double app = e[p]; 
                                double apq = A[p,q]; 
                                double aqq = e[q]; 
                                double phi = 0.5*Atan2(2*apq,aqq-app)+0.5*PI; 
                                double c = Cos(phi), s = Sin(phi); 
                                double appm = c*c*app-2*c*s*apq+s*s*aqq; 
                                double aqqm = c*c*aqq+2*c*s*apq+s*s*app; 
                                if(appm!=app){ 
                                        changed = 1; 
                                        e[p]=appm; 
                                        e[q] = aqqm; 
                                        A[p,q] = 0.0; 
                                        for(int i = 0; i<p; i++){ 
                                                double aip = A[i,p]; 
                                                double aiq = A[i,q]; 
                                                A[i,p] = c*aip-s*aiq; 
                                                A[i,q] = c*aiq+s*aip; 
                                        } 
                                        for(int i = p+1; i<q; i++){ 
                                                double api = A[p,i]; 
                                                double aiq = A[i,q]; 
                                                A[p,i] = c*api-s*aiq; 
                                                A[i,q] = c*aiq+s*api; 
                                        } 
                                        for(int i = q+1; i<A.size1; i++){ 
                                                double api = A[p,i]; 
                                                double aqi = A[q,i]; 
                                                A[p,i] = c*api-s*aqi; 
                                                A[q,i] = c*aqi+s*api; 
                                        } 
                                        for(int i = 0; i<A.size1; i++){ 
                                                double vip = V[i,p]; 
                                                double viq = V[i,q]; 
                                                V[i,p] = c*vip-s*viq; 
                                                V[i,q] = c*viq+s*vip; 
                                        } 
 
                                } 
                        } 
                }} 
                return sweep; 
        };
}
