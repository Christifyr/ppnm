using System;
using static System.Math;
using static System.Console;
class main{
	public static void Main(){
	int n=99;
	double s=1.0/(n+1);
	matrix H = new matrix(n,n);
	H.set_identity();
	for(int i=0;i<n-1;i++){
  		H[i,i] = -2;
  		H[i,i+1] = 1;
  		H[i+1,i] = 1;
  	}
	H[n-1,n-1] = -2;
	matrix.scale(H,-1/s/s);
	matrix V = new matrix(n,n);
	JacobiEigen.cyclicDecomp(H,V);
	for (int k=0; k < Min(n/3,30); k++){
    		double exact = PI*PI*(k+1)*(k+1);
    		double calculated = H[k,k];
    		WriteLine($"{k} {calculated} {exact}");
	}
	WriteLine();
	for(int k=0;k<5;k++){
  		WriteLine($"{0} {0}");
  		for(int i=0;i<n;i++)
			WriteLine($"{(i+1.0)/(n+1)} {matrix.get(V,i,k)}");
  		WriteLine($"{1} {0}");
		WriteLine();
  	}
	}
}
