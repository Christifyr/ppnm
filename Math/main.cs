using static System.Console;
using static cmath;
using static complex;
using static System.Math;
class Math{
	static void Main(){
		complex x = new complex(2,0);
		complex z = new complex(0,1);
		complex zp= new complex(0,-1);
		complex y = sqrt(x);
		complex t = new complex(-1,0);
		Write("sqrt({0})={1:D2}\n",x,sqrt(x));
		Write("sqrt({0})*sqrt({0})={1:D2}\n",x,y*y);
		Write("e^({0})={1:D2}\n",z,exp(z));
		Write("e^({0})*e^({1})={2:D2}\n",z,zp,exp(z)*exp(zp));
		Write("e^({0}pi)={1:D2}\n",z,exp(z*PI));
		Write("e^({0}pi)*e^({1}pi)={2:D2}\n",z,zp,exp(z*PI)*exp(zp*PI));
		Write("{0}^{0}={1:D2}\n",z,cmath.pow(z,z));
		Write("{0}^{0}*{0}^{1}={2:D2}\n",z,zp,cmath.pow(z,z)*cmath.pow(z,zp));
		Write("sin({0}pi)={1}\n",z,sin(z*PI));
		Write("sin({0}pi)^2+cos({0}pi)={1:D2}\n",z,cmath.pow(sin(z*PI),2)+cmath.pow(cos(z*PI),2));
		Write("sqrt({0})={1:D2}\n",t,sqrt(t));
		Write("sqrt({0})*sqrt({0})={1:D2}\n",t,sqrt(t)*sqrt(t));
		Write("sinh({0})={1:D2}\n",z,sinh(z));
		Write("cosh({0})={1:d2}\n",z,cosh(z));
		Write("sinh({0})^2+cosh({0})^2={1:D2}\n",z,cmath.pow(sinh(z),2)+cmath.pow(cosh(z),2));
		Write("sqrt({0})={1:D2}\n",z,sqrt(z));
	}
}

