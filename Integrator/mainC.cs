using System;
using static System.Math;
using static System.Console;
class main{
	public static void Main(){
		Func<double,double> myfun = (x) => Exp(-Pow(x,2));
		Func<double,double> myfun2 = (x) => Pow(x,2)*Exp(-Pow(x,2));
		double a = -System.Double.PositiveInfinity;
		double b = System.Double.PositiveInfinity;
		double acc = 1e-4;
		double eps = 1e-4;
		WriteLine(quadint.g20k41av(myfun,a,b,acc,eps,true));
		WriteLine(quadint.g20k41av(myfun2,0,b,acc,eps,true));
	}
}
