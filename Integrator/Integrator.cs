using System;
using static System.Math;
using static System.Double;
public class quadint{
	private static double g20k41a(Func<double,double> f,double a, double b, double acc, double eps, double f1=NaN, double f2=NaN,double f3=NaN,double f4=NaN,double f5=NaN,double f6=NaN,double f7=NaN,double f8=NaN,double f9=NaN,double f10=NaN){
		// Theese are for the interval [-1,1]
		double x0 = 0.000000000000000000000000000000000e+00, kw0 = 7.660071191799965644504990153010174e-02;
		double x1 = 7.652652113349733375464040939883821e-02, gw1 = 1.527533871307258506980843319550976e-01, kw1 = 7.637786767208073670550283503806100e-02;
                double x2 = 2.277858511416450780804961953685746e-01, gw2 = 1.491729864726037467878287370019694e-01, kw2 = 7.458287540049918898658141836248753e-02; 
                double x3 = 3.737060887154195606725481770249272e-01, gw3 = 1.420961093183820513292983250671649e-01, kw3 = 7.105442355344406830579036172321017e-02; 
                double x4 = 5.108670019508270980043640509552510e-01, gw4 = 1.316886384491766268984944997481631e-01, kw4 = 6.583459713361842211156355696939794e-02; 
                double x5 = 6.360536807265150254528366962262859e-01, gw5 = 1.181945319615184173123773777113823e-01, kw5 = 5.911140088063957237496722064859422e-02; 
                double x6 = 7.463319064601507926143050703556416e-01, gw6 = 1.019301198172404350367501354803499e-01, kw6 = 5.094457392372869193270767005034495e-02; 
                double x7 = 8.391169718222188233945290617015207e-01, gw7 = 8.327674157670474872475814322204621e-02, kw7 = 4.166887332797368626378830593689474e-02; 
                double x8 = 9.122344282513259058677524412032981e-01, gw8 = 6.267204833410906356950653518704161e-02, kw8 = 3.128730677703279895854311932380074e-02; 
                double x9 = 9.639719272779137912676661311972772e-01, gw9 = 4.060142980038694133103995227493211e-02, kw9 = 2.038837346126652359801023143275471e-02; 
                double x10 = 9.931285991850949247861223884713203e-01, gw10 = 1.761400713915211831186196235185282e-02, kw10 = 8.600269855642942198661787950102347e-03; 
                double x11 = 1.526054652409226755052202410226775e-01, kw11 = 7.570449768455667465954277537661656e-02; 
                double x12 = 3.016278681149130043205553568585923e-01, kw12 = 7.303069033278666749518941765891311e-02; 
                double x13 = 4.435931752387251031999922134926401e-01, kw13 = 6.864867292852161934562341188536780e-02; 
                double x14 = 5.751404468197103153429460365864251e-01, kw14 = 6.265323755478116802587012217425498e-02; 
                double x15 = 6.932376563347513848054907118459315e-01, kw15 = 5.519510534828599474483237241977733e-02;
		double x16 = 7.950414288375511983506388332727879e-01, kw16 = 4.643482186749767472023188092610752e-02;
		double x17 = 8.782768112522819760774429951130785e-01, kw17 = 3.660016975820079803055724070721101e-02;
		double x18 = 9.408226338317547535199827222124434e-01, kw18 = 2.588213360495115883450506709615314e-02;
		double x19 = 9.815078774502502591933429947202169e-01, kw19 = 1.462616925697125298378796030886836e-02;
		double x20 = 9.988590315882776638383155765458630e-01, kw20 = 3.073583718520531501218293246030987e-03;
		double scale = (b-a)/2.0;
		double f2n, f4n, f6n, f8n, f10n;
		if(IsNaN(f1)){
			f2 = f(a+scale*(x2+1)); f4 = f(a+scale*(x4+1)); f6 = f(a+scale*(x6+1)); f8 = f(a+scale*(x8+1)); f10 = f(a+scale*(x10+1));
			f2n = f(a+scale*(-x2+1)); f4n = f(a+scale*(-x4+1)); f6n = f(a+scale*(-x6+1)); f8n = f(a+scale*(-x8+1)); f10n = f(a+scale*(-x10+1));
		}
		else{
			f10n = f1; f8n = f2; f6n = f3; f4n = f4; f2n = f5; 
			f2 = f6; f4 = f7; f6 = f8; f8 = f9;
		}
		double f0 = f(a+scale*(x0+1));
		f1 = f(a+scale*(x1+1)); f3 = f(a+scale*(x3+1)); f5 = f(a+scale*(x5+1)); f7 = f(a+scale*(x7+1)); f9 = f(a+scale*(x9+1));
		double f1n = f(a+scale*(-x1+1)), f3n = f(a+scale*(-x3+1)), f5n = f(a+scale*(-x5+1)), f7n = f(a+scale*(-x7+1)), f9n = f(a+scale*(-x9+1));
                double f11 = f(a+scale*(x11+1)), f12 = f(a+scale*(x12+1)), f13 = f(a+scale*(x13+1)), f14 = f(a+scale*(x14+1)), f15 = f(a+scale*(x15+1));
		double f16 = f(a+scale*(x16+1)), f17 = f(a+scale*(x17+1)), f18 = f(a+scale*(x18+1)), f19 = f(a+scale*(x19+1)), f20 = f(a+scale*(x20+1));
		double f11n = f(a+scale*(-x11+1)), f12n = f(a+scale*(-x12+1)), f13n = f(a+scale*(-x13+1)), f14n = f(a+scale*(-x14+1)), f15n = f(a+scale*(-x15+1));
		double f16n = f(a+scale*(-x16+1)), f17n = f(a+scale*(-x17+1)), f18n = f(a+scale*(-x18+1)), f19n = f(a+scale*(-x19+1)), f20n = f(a+scale*(-x20+1));
                double Q = (f0*kw0+f1*kw1+f2*kw2+f3*kw3+f4*kw4+f5*kw5+f6*kw6+f7*kw7+f8*kw8+f9*kw9+f10*kw10+f11*kw11+f12*kw12+f13*kw13+f14*kw14+f15*kw15+f16*kw16+f17*kw17+f18*kw18+f19*kw19+f20*kw20)*scale; 
                double q = (f1*gw1+f2*gw2+f3*gw3+f4*gw4+f5*gw5+f6*gw6+f7*gw7+f8*gw8+f9*gw9+f10*gw10)*scale;
		Q += (f1n*kw1+f2n*kw2+f3n*kw3+f4n*kw4+f5n*kw5+f6n*kw6+f7n*kw7+f8n*kw8+f9n*kw9+f10n*kw10+f11n*kw11+f12n*kw12+f13n*kw13+f14n*kw14+f15n*kw15+f16n*kw16+f17n*kw17+f18n*kw18+f19n*kw19+f20n*kw20)*scale;       
                q += (f1n*gw1+f2n*gw2+f3n*gw3+f4n*gw4+f5n*gw5+f6n*gw6+f7n*gw7+f8n*gw8+f9n*gw9+f10n*gw10)*scale;       
                double tolerance = acc+eps*Abs(Q), error = Abs(Q-q); 
                if(error<tolerance) return Q; 
                else return g20k41a(f,(a+b)/2.0,b,acc/Sqrt(2.0),eps,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10) + g20k41a(f,a,(a+b)/2.0,acc/Sqrt(2.0),eps,f10n,f9n,f8n,f7n,f6n,f5n,f4n,f3n,f2n,f1n);
	}	

	public static Func<Func<double,double>,double,double,double,double,bool,double> g20k41av = (f,a,b,acc,eps,transform) =>{
		if(IsNegativeInfinity(a) && IsPositiveInfinity(b)) return g20k41a(clenscurttransform(t=>f(t/(1-Pow(t,2)))*(1+Pow(t,2))/Pow((1-Pow(t,2)),2)),0,PI,acc,eps);
		if(IsPositiveInfinity(b)) return g20k41a(t=>f(a+t/(1-t))/Pow((1-t),2),0,1,acc,eps);
		if(IsNegativeInfinity(a)) return g20k41a(t=>f(b-(1-t)/t)/Pow(t,2),0,1,acc,eps);
		if(transform) return g20k41a(clenscurttransform(t=>f(a+(b-a)/2.0*(t+1))/2.0),0,PI,acc,eps);
		return g20k41a(t=>f(a+(b-a)/2.0*(t+1))/2,-1,1,acc,eps);
	};

	public static Func<Func<double,double>,Func<double,double>> clenscurttransform = (f) => {
		return (theta) => f(Cos(theta))*Sin(theta);
	};
}
