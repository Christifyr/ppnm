using System;
using static System.Math;
using static System.Console;
class main{
	public static void Main(){
		Func<double,double> myfun = (x) => Sqrt(x);
		Func<double,double> myfun2 = (x) => 4*Sqrt(1-Pow(x,2));
		double a = 0.0;
		double b = 1.0;
		double acc = 1e-6;
		double eps = 1e-6;
		WriteLine(quadint.g20k41av(myfun,a,b,acc,eps,false));
		WriteLine(quad.o8av(myfun,a,b,acc,eps));
		WriteLine(quadint.g20k41av(myfun2,a,b,acc,eps,false));
                WriteLine(quad.o8av(myfun2,a,b,acc,eps));
	}
}
