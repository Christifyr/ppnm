using static System.Math;
public struct myapprox{
	public static bool approx(double a, double b, double tau=1e-9, double eps=1e-9){
		return Abs(a-b) < tau || Abs(a-b)/(Abs(a)+Abs(b)) < eps/2;
	}
}
