using System;
using static System.Math;
using System.Collections.Generic;
public struct ODE{
	private static Func<Func<double,vector,vector>,double,vector,double,List<vector>> rkstep45 = (f,t,yt,h) => {
		vector k0 = f(t,yt);
		vector k1 = f(t+h/4,yt+(1.0/4)*h*k0);
		vector k2 = f(t+3*h/8,yt+3*h/32*k0+9*h/32*k1);
		vector k3 = f(t+12*h/13,yt+1932*h/2197*k0-7200*h/2197*k1+7296*h/2197*k2);
		vector k4 = f(t+h,yt+439*h/216*k0-8*h*k1+3680*h/513*k2-845*h/4104*k3);
		vector k5 = f(t+h/2,yt-8*h/27*k0+2*h*k1-3544*h/2565*k2+1859*h/4104*k3-11*h/40*k4);
		vector ka = 16*k0/135+6656*k2/12825+28561*k3/56430-9*k4/50+2*k5/55;
		vector kb = 25*k0/216+1408*k2/2560+2197*k3/4104-1*k4/5;
		vector yh = yt+ka*h;
		vector err = (ka-kb)*h;
		return new List<vector>(){yh,err};		
	};

	public static Action<Func<double,vector,vector>,double,double,vector,double,List<double>,List<vector>,double,double> ode45 = (f,a,b,y0,h,xlist,ylist,acc,eps) =>{
		int k=0;
		xlist.Add(a);
		ylist.Add(y0);
		while(xlist[k]<b){
			double x = xlist[k]; 
			vector y = ylist[k];
			List<vector> list = rkstep45(f,x,y,h);
			vector yh = list[0];
			vector err = list[1];
			vector tol = new vector(err.size);
			for(int i =0; i<tol.size;i++){
				tol[i] = (eps*Abs(yh[i])+acc)*Sqrt(h/(b-a));
				if(err[i]==0) err[i]=tol[i]/4;
			}
			bool stepok = true;
			for(int i=0; i<tol.size;i++){
				if(Abs(err[i])>tol[i]) stepok=false;
			}
			if(stepok){
				k++;
				xlist.Add(x+h);
				ylist.Add(yh);
			}
			double factor = tol[0]/Abs(err[0]);
			for(int i = 0; i<tol.size;i++){
				factor = Min(factor,tol[i]/Abs(err[i]));
			}
			h*=Min(2,Pow(factor,0.25)*0.95);
		}
	};
}
