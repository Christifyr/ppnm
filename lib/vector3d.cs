using System;
using SM=System.Math;
using System.Globalization;
public partial struct vector3d : ivector{
	public double x,y,z;
	public double X{get{return x;} set{x=value;}}
	public double Y{get{return y;} set{y=value;}}
	public double Z{get{return z;} set{z=value;}}
	public vector3d(double x, double y, double z){
		this.x=x;this.y=y;this.z=z;
	}
	//Operators:
	//+
	public static vector3d operator+(vector3d v1, vector3d v2){
		return new vector3d(v1.X+v2.X,v1.Y+v2.Y,v1.Z+v2.Z);
	}
	public static vector3d operator+(vector3d v, double d){
		return new vector3d(v.X+d,v.Y+d,v.Z+d);
	}
	public static vector3d operator+(double d, vector3d v){
		return v+d;
	}

	//-
	public static vector3d operator-(vector3d v1,vector3d v2){
		return new vector3d(v1.X-v2.X,v1.Y-v2.Y,v1.Z-v2.Z);
	}
	public static vector3d operator-(vector3d v, double d){
		return new vector3d(v.X-d,v.Y-d,v.Z-d);
	}
	public static vector3d operator-(double d,vector3d v){
		return new vector3d(d-v.X,d-v.Y,d-v.Z);
	}

	//*
	public static vector3d operator*(vector3d v, double d){
		return new vector3d(v.X*d,v.Y*d,v.Z*d);
	}
	public static vector3d operator*(double d, vector3d v){
		return v*d;
	}

	// /
	public static vector3d operator/(vector3d v, double d){
		return new vector3d(v.X/d,v.Y/d,v.Z/d);
	}
	public static vector3d operator/(double d, vector3d v){
		return v/d;
	}
	
	public double dot(ivector v){
		return this.X*v.X+this.Y*v.Y+this.Z*v.Z;
	}
	public ivector cross(ivector v){
		return new vector3d(this.Y*v.Z-this.Z*v.Y,this.Z*v.X-this.X*v.Z,this.X*v.Y-this.Y*v.X);
	}
	
	public double magnitude(){
		return this.dot(this);
	}

	public override string ToString(){
		return String.Format(
				CultureInfo.CurrentCulture,
				"[{0},{1},{2}]",this.X,this.Y,this.Z);
	}

	public override bool Equals(System.Object obj){
		if(obj is vector3d){
			vector3d o = (vector3d)obj;
			if(o.X==this.X && o.Y==this.Y && o.Z==this.Z){
				return true;
			}
		}
		return false;
	}
	public override int GetHashCode(){
		throw new System.NotImplementedException("Don't care");
	}
}
