public interface ivector{
	double X{get; set;}
	double Y{get; set;}
	double Z{get; set;}
	double dot(ivector v);
	ivector cross(ivector v);
}
