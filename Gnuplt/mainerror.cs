using static System.Console;
using static System.Math;
public class main{
	public static void Main(){
		double dx = 1.0/16;
		for(double x=-3.0; x<= 3.0; x+=dx){
			WriteLine("{0,11:f3} {1,15:f3}",x,math.erf(x));
		}
	}
}

