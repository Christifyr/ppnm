using static System.Console;
class main{
	public static void Main(){
		double eps = 1.0/32; double dx = 1.0/16;
		for(double x = -4+eps; x<= 4; x+=dx){
			for(double y = -4+eps; y<= 4; y+=dx){
				WriteLine("{0}	{1}	{2}",x,y,cmath.abs(math.gamma(new complex(x,y))));
			}
		}
	}
}
