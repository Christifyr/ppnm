using System;
using static System.Math;
using static System.Console;
public class main{
        public static void Main(){
                double eps=1.0/32, dx=1.0/16;
		for(double x=-4+eps;x<=4;x+=dx){
                        WriteLine("{0,11:f3} {1,15:f3}",x,math.gamma(x));
                }
	}
}

