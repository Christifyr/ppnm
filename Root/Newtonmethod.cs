using System;
using static System.Math;
public static class newton{

	private static Func<Func<vector,vector>,vector,vector,double,vector> jacobi = (f,z,fz,dx) => {
		vector x = z.copy();
		vector fx = fz.copy();
		int n = x.size;
		matrix J = new matrix(n,n);
		for(int i = 0; i<n; i++){
			x[i]+=dx;
			vector df = f(x)-fx;
			for(int j=0;j<n;j++){
				J[j][i] = df[j]/dx;
			}
			x[i]-=dx;
		}
		matrix R = new matrix(n,n);
		QRMaker.qr_gs_decomp(J,R);
		vector sol = QRMaker.qr_gs_solve(J,R,-1.0*fx);	
		return sol;
	};

	public static Func<Func<vector,vector>,vector,double,double,vector> newtonmethod = (f,x,eps,dx) => {
		vector fx = f(x);
		bool converged = false;
		while(!converged){
			vector Dx = jacobi(f,x,fx,dx);
			double lambda = 1.0;
			while(f(x+lambda*Dx).norm()>(1-lambda/2)*fx.norm() && lambda>1.0/32){
				lambda = lambda/2;
			}
			x = x+lambda*Dx;
			fx = f(x);
			if(fx.norm()<eps){
			converged = true;}
		}
		return x;
	};

}
