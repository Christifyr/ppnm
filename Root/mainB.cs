using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
public static class main{
	public static void Main(){
		double rmax = 10.0;
		Func<vector,vector> aux = (energy) =>{
			double solution = hydrogen.rad(energy[0],rmax,40e-3,40e-3);
			return new vector(solution);
		};
		vector energystart = new vector(1);
		energystart[0]= -1.0;
		vector energyfound=newton.newtonmethod(aux,energystart,1e-4,1e-7);
		double energygot = energyfound[0];
		double energyexact = -0.5;
		for(double r=0;r<rmax;r+=0.1){
			WriteLine($"{r}	{hydrogen.rad(energygot,r,1e-5,1e-5)}	{r*Exp(-r)}");
		}
		
	}
}
	
public class hydrogen{
	public static double rad(double energy,double r,double eps,double acc){
		double rmin = 1e-3;
		if(r<rmin) return r-r*r;
		Func<double,vector,vector> fe = (x,y) =>{
			return new vector(y[1],2*(-energy*y[0]-y[0]/x));
		};
		vector rstart = new vector(rmin-rmin*rmin,1-2*rmin);
		List<double> xlist = new List<double>();
		List<vector> ylist = new List<vector>();
		ODE.ode45(fe,rmin,r,rstart,0.001,xlist,ylist,eps,acc);
		return ylist[ylist.Count-1][0];
	}

}
