using System;
using static System.Math;
using static System.Console;
public static class main{
	public static void Main(){
		Func<vector,vector> f = (y) => new vector(Pow(y[0],2)-16,Pow(y[1],2)-9);
		Func<vector,double> rosen = (y) => Pow((1-y[0]),2)+100*Pow((y[1]-Pow(y[0],2)),2);
		Func<vector,vector> gradRosen = (y) => new vector(2*(200*Pow(y[0],3)-200*y[0]*y[1]+y[0]-1),200*(y[1]-Pow(y[0],2)));
		vector x = new vector(2);
		x[0]=500;
		x[1]=500;
		vector xx=newton.newtonmethod(f,x,1e-4,1e-7);
		xx.print("The test values found are: ");
		f(xx).print("checking that it is indeed zero: ");
		x[0]=2;
		x[1]=2;
		vector xxx = newton.newtonmethod(gradRosen,x,1e-4,1e-7);
		xxx.print("The extremums values found are : ");
		gradRosen(xxx).print("Checking that they are indeed zero: ");
		WriteLine($"The value are then {rosen(xxx)}");
	}
}
