using System;
using static System.Console;
using static System.Math;
class main{
	public static double inf = System.Double.PositiveInfinity;	
	public static void Main(){
		double p = 2;
		Func<double,double> f1 = (x) => Log(x)/Sqrt(x);
		Func<double,double> f2 = (x) => Exp(-Pow(x,2));
		Func<double,double> f3 = (x) => Pow(Log(1/x),p);
		double firstVal = quad.o8av(f1,0,1);
		double secondVal = quad.o8av(f2,-inf,inf);
		double thirdVal = quad.o8av(f3,0,1);
		WriteLine($"int ln(x)/sqrt(x) dx from 0 to inf is {firstVal}");
		WriteLine($"int exp(-x^2) dx from -inf to inf is {secondVal} and sqrt(PI) is {Sqrt(PI)}");
		WriteLine($"int (ln(1/x))^2 dx from 0 to 1 is {thirdVal} and Gamma(2+1)! is {2*1}");
	}
}
