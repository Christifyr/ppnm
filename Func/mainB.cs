using System;
using static System.Math;
using static System.Console;
class main{
	public static double inf = System.Double.PositiveInfinity;
	public static void Main(){
		Func<double,double> f = (x) => Pow(x,2)*Exp(-Pow(x,2));
		Func<double,double> f2 = (x) => Pow(Sin(x),2)/Pow(x,2);
		Func<double,double> f3 = (x) => x/(Exp(x)-1);
		Func<double,double> f4 = (x) => Pow(x,3)/(Exp(x)-1);
		double a = 0;
		double b = inf;
		WriteLine($"int x^2*e^(-x^2) dx from 0 to inf is {quad.o8av(f,a,b)} and 1/4*sqrt(pi) is {1.0/4*Sqrt(PI)}");
		WriteLine($"int sin^2(x)/x^2 dx from 0 to inf is {quad.o8av(f2,a,b)} and pi/2 is {PI/2}");
		WriteLine($"int x/(e^x-1) dx from 0 to inf is {quad.o8av(f3,a,b)} and pi^2/6 is {Pow(PI,2)/6}");
		WriteLine($"int x^3/(e^x-1) dx from 0 to inf is {quad.o8av(f4,a,b)} and pi^4/15 is {Pow(PI,4)/15}");
	}
}
