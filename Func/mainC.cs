using System;
using static System.Math;
using static System.Console;
class main{
	public static double inf = System.Double.PositiveInfinity;
	
	public static double energy(double a){
		Func<double,double> norm = (x) => Exp(-a*Pow(x,2));
		Func<double,double> ham = (x) => (-Pow(a,2)*Pow(x,2)/2+a/2+Pow(x,2)/2)*Exp(-a*Pow(x,2));
		return quad.o8av(ham,-inf,inf)/quad.o8av(norm,-inf,inf);
	}
	
	public static void Main(){
			for(double a = 0; a<=10; a+=1.0/16){
			WriteLine($"{a}	{energy(a)}");
			}
	}
}
