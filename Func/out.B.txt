int x^2*e^(-x^2) dx from 0 to inf is 0.443113105376286 and 1/4*sqrt(pi) is 0.443113462726379
int sin^2(x)/x^2 dx from 0 to inf is 1.57075853274621 and pi/2 is 1.5707963267949
int x/(e^x-1) dx from 0 to inf is 1.64493415408091 and pi^2/6 is 1.64493406684823
int x^3/(e^x-1) dx from 0 to inf is 6.49393737654557 and pi^4/15 is 6.49393940226683
