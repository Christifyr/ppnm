using System;
class maincmd{
	public static void Main(string[] args){
		var stdout = Console.Out;
		foreach(string s in args){
			double x = double.Parse(s);
			stdout.WriteLine("{0} {1} {2}",x,Math.Sin(x),Math.Cos(x));
		}
	}
}
