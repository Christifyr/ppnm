using System;
class mainRFF{
	public static void Main(string[] args){
		var ind = new System.IO.StreamReader(args[0]);
		var outp = new System.IO.StreamWriter(args[1],append:true);
		string s;
		while((s=ind.ReadLine()) != null){
			double x = double.Parse(s);
			outp.WriteLine("{0} {1} {2}",x,Math.Sin(x),Math.Cos(x));
		}
		ind.Close();
		outp.Close();
	}
}
