using System;
class mainstd{
	public static void Main(){
		var stdin = Console.In;
		var stdout = Console.Out;
		do {
			string s = stdin.ReadLine();
			if(s==null) break;
			double x = double.Parse(s);
			stdout.WriteLine("{0} {1} {2}",x,Math.Sin(x),Math.Cos(x));
		}
		while(true);	
	}
}
