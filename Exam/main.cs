using System;
using static System.Math;
using static System.Console;
class main{
	public static void Main(){
		int n = 4;
		matrix A=new matrix(n,n);
		matrix U=new matrix(n,n);
		matrix V=new matrix(n,n);
		vector b=new vector(n);
		var rand = new Random();
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				A[i,j]=rand.NextDouble();
			}
			b[i]=rand.NextDouble();
		}
		matrix I = new matrix(n,n);
		I.set_identity();
		A.print("The matrix we want to bidiagonalise is:");
		matrix B=gkl.bidigonalisation(A,U,V);
		B.print("Checking that the matrix is diagonalised:");
		WriteLine($"UU.T=I? {I.approx(U*U.T)}");
		WriteLine($"VV.T=I? {I.approx(V*V.T)}");
		WriteLine($"A=UBV.T? {A.approx(U*B*V.T)}");
		b.print("The vector solution we want");	
		vector x = gkl.solve(B,U,V,b);
		x.print("The variables we get that solves Ax=b");
		(A*x).print("The approx for vector is a bit funky but A*x=");
		WriteLine("Moving on to finding the inverse");
		matrix Ai=new matrix(n,n);
		gkl.Ainverter(B,U,V,Ai);
		Ai.print("The inverse of a is:");
		WriteLine($"A*Ai=I? {I.approx(A*Ai)}");
		WriteLine("Moving on to the determinant");
		WriteLine("Since U and V are orthogonal (det(U))^2=(det(V))^2=1 we get");
		WriteLine("det(A)=det(UBV.T)=det(U)det(B)det(V.T)=det(B)");
		WriteLine("The determination of a bidiagonal matrix is the product of the diagonals, the determinat is therefore");
		WriteLine($"det(A)={gkl.det(B)}");
	}
}
