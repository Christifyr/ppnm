using System;
using static System.Math;
using static System.Console;
public struct gkl{
	public static matrix bidigonalisation(matrix A, matrix U, matrix V){
		int size = A.size1;
		matrix B = new matrix(size,size);
		double[] beta = new double[size];
		double[] alpha = new double[size];
		U.set_identity(); 
                V.set_identity();
		for(int i=0; i<size;i++){ 
                        V[i,0]=Sqrt(1.0/size); 
                } 
		B.set_zero();
		U[0]=A*V[0];
		alpha[0]=U[0].norm();
		U[0]/=alpha[0];
		V[1]=A.T*U[0]-alpha[0]*V[0];
		beta[0]=V[1].norm();
		V[1]/=beta[0];
		B[0,0]=alpha[0];
		B[0,1]=beta[0];
		for(int k=1;k<size-1;k++){
			U[k]=A*V[k]-beta[k-1]*U[k-1];
                	alpha[k]=U[k].norm();
               		U[k]=U[k]/alpha[k];
                	V[k+1]=A.T*U[k]-alpha[k]*V[k];
			beta[k]=V[k+1].norm();
			V[k+1]=V[k+1]/beta[k];
			B[k,k+1]=beta[k];
			B[k,k]=alpha[k];
		}
		U[size-1]= A*V[size-1]-beta[size-2]*U[size-2];
		alpha[size-1]=U[size-1].norm();
		U[size-1]/=alpha[size-1];
		V[size-1]=A.T*U[size-1]/alpha[size-1];
		B[size-1,size-1]=alpha[size-1];
		return B;
	}
	
	public static vector solve(matrix B, matrix U, matrix V,vector y){
		matrix inverse = Binverter(B);
		return V*inverse*U.T*y;
	}

	public static double det(matrix B){
		double prod = 1;
		for(int i=0;i<B.size1;i++){
			prod*=B[i,i];
		}
		return prod;
	}
	
	public static void Ainverter(matrix B, matrix U,matrix V,matrix Ai){
		int n=B.size1;
		Ai.set_identity();
		for(int i=0; i<n;i++){
			Ai[i]=solve(B,U,V,Ai[i]);
		}
	}
	
	public static matrix Binverter(matrix B){
		int n = B.size1;
		matrix Bi = new matrix(n,n);
		for(int i=0;i<n;i++){
			Bi[i,i]=1.0/B[i,i];
			if(i!=n-1){
			Bi[i,i+1]=-B[i,i+1]*Bi[i,i]/B[i+1,i+1];}
			for(int j=i+2;j<n;j++){
				Bi[i,j]=-Bi[i,j-1]*B[j-1,j]/B[j,j];
			}
		}
		return Bi;
	}
}
