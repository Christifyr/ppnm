using System;
using static System.Math;
using static System.Console;
class main{
	public static void Main(){
		var rand = new Random();
		int m = rand.Next(1,101);
		int n = m;
		matrix A = new matrix(n,m);
		vector b = new vector(n);
		for(int i = 0;i<n;i++){
			for(int j = 0; j<m;j++){
				A[i,j] = rand.NextDouble();
			}
			b[i] = rand.NextDouble();
		}
		matrix R = new matrix(m,m);
		QRMaker.qr_gs_decomp(A,R);
		matrix P = A.T*A;
		matrix test1 = new matrix(n,m);
		test1.set_identity();
		WriteLine(P.approx(test1));
		for(int i = 0; i<R.size1;i++){
			for(int j = i+1; j<R.size2;j++){
				if(Round(R[j,i],14,MidpointRounding.AwayFromZero) !=0){
					WriteLine("The matrix R is not upper triangular");
				}
			}
		}
		vector x = QRMaker.qr_gs_solve(A,R,b);
		vector test2 = A*R*x;
		WriteLine(test2.approx(b));
	}
}
