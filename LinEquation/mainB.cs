using System;
using static System.Math;
using static System.Console;
class main{
        public static void Main(){
                var rand = new Random();
                int m = rand.Next(1,101);
                int n = m;
                matrix A = new matrix(n,m);
                vector b = new vector(n);
                for(int i = 0;i<n;i++){
                        for(int j = 0; j<m;j++){
                                A[i,j] = rand.NextDouble();
                        }
                        b[i] = rand.NextDouble();
                }
		matrix AA = A.copy(), B = A.copy();
                matrix R = new matrix(m,m);
                QRMaker.qr_gs_decomp(A,R);
                QRMaker.qr_gs_inverse(A,R,B);
                matrix test1 = new matrix(n,m);
                test1.set_identity();
                WriteLine((AA*B).approx(test1));
        }
}

