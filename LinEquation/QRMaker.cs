using System;
using static System.Math;
using static System.Console;
public struct QRMaker{
	public static Action<matrix,matrix> qr_gs_decomp = (A,R) => {
		R.set_zero();
		for(int i = 0; i<A.size2; i++){
			R[i,i] = A[i].norm();
			A[i] = A[i]/R[i,i];
			for(int j = i+1; j<A.size2;j++){
				R[i,j] = A[i].dot(A[j]);
				A[j]-=R[i,j]*A[i];
			}
		}
	};

	public static Func<matrix,matrix,vector,vector> qr_gs_solve = (Q,R,b) => {
		vector a = Q.T*b;
		for(int i = a.size-1; i>=0; i--){
			double s = a[i];
			for(int j = i+1; j<a.size; j++){
				s-=R[i,j]*a[j];
			}
		a[i] = (s)/R[i,i];}
	return a;
	};

	public static Action<matrix,matrix,matrix> qr_gs_inverse = (Q,R,B) => {
		B.set_identity();
		for(int i = 0; i<Q.size2;i++){
			B[i]=qr_gs_solve(Q,R,B[i]);
		}
	};

	public static Action<matrix> qr_gr_decomp = (A) => {
		for(int i = 0; i<A.size2; i++){
			for(int j = i+1; j<A.size1;j++){
				double theta = Atan2(A[j,i],A[i,i]);
				for(int k = i; k<A.size2;k++){
					double xp = A[j,k], xq = A[i,k];
					A[i,k] = xq*Cos(theta)+xp*Sin(theta);
					A[j,k] = xp*Cos(theta)-xq*Sin(theta);
				}
				A[j,i] = theta;
			}
		}
	};

	public static Action<matrix,vector> qr_gr_bMaker = (QR,b) => {
		for(int i = 0; i<QR.size2; i++){
			for(int j = i+1; j<QR.size1; j++){
				double theta = QR[j,i], vq = b[i], vp=b[j];
				b[i]=vq*Cos(theta)+vp*Sin(theta);
				b[j]=vp*Cos(theta)-vq*Sin(theta);
			}
		}
	};

	public static Func<matrix,vector,vector> qr_gr_solve = (QR,b) => {
	vector a = b.copy();
	qr_gr_bMaker(QR,a);
                for(int i = a.size-1; i>=0; i--){
                        double s = a[i];
                        for(int j = i+1; j<a.size; j++){
                                s-=QR[i,j]*a[j];
                        }
                a[i] = (s)/QR[i,i];}
        return a;
	};
}
