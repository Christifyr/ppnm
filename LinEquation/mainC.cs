using System;
using static System.Math;
using static System.Console;
class main{
        public static void Main(){
                var rand = new Random();
                int m = rand.Next(1,101);
                int n = m;
                matrix A = new matrix(n,m);
                vector b = new vector(n);
                for(int i = 0;i<n;i++){
                        for(int j = 0; j<m;j++){
                                A[i,j] = rand.NextDouble();
                        }
                        b[i] = rand.NextDouble();
                }
		matrix AA = A.copy();
                QRMaker.qr_gr_decomp(A);
                vector x = QRMaker.qr_gr_solve(A,b);
		vector test = AA*x;
                WriteLine(test.approx(b));
        }
}

