using System;
using static System.Math;
using System.Collections.Generic;
public class quadIntrapol{
	List<double> xs;
	List<double> ys;
	List<double> p;
	List<double> b;
	List<double> c;

	public quadIntrapol(List<double> x, List<double> y){
		this.xs = x;
		this.ys = y;
		p = new List<double>();
		c = new List<double>();
		b = new List<double>();
		for(int i = 0; i< xs.Count-1; i++){
			p.Add((ys[i+1]-ys[i])/(xs[i+1]-xs[i]));
		}
		c.Add(0);
		for(int i = 0; i< xs.Count-2; i++){
			c.Add((p[i+1]-p[i]-c[i]*(xs[i+1]-xs[i]))/(xs[i+2]-xs[i+1]));
		}
		c[c.Count-1] = c[c.Count-1]/2;
		for(int i = c.Count-1; i>0;i--){
			c[i-1] = (p[i]-p[i-1]-c[i]*(xs[i+1]-xs[i]))/(xs[i]-xs[i-1]);
		}
		for(int i = 0; i< c.Count; i++){
			b.Add(p[i]-c[i]*(xs[i+1]-xs[i]));
		}
	}
	
	private int binsearch(double z){
		int i = 0;
                int j = xs.Count-1;
                while(j-i>1){
                        int m = (j+i)/2;
                        if(z-xs[m] > 0){ i = m;}
                        else{j=m;}
                }
		return i;}
	
	public double quadterp(double z){
		int i = binsearch(z);
		return ys[i]+b[i]*(z-xs[i])+c[i]*Pow(z-xs[i],2);
	}
	
	public double quadterpIntegral(double z){
		int j = binsearch(z);
                double sum = 0;
                for(int i = 0; i<j;i++){
                        sum += ys[i]*(xs[i+1]-xs[i])+b[i]*Pow(xs[i+1]-xs[i],2)/2+c[i]*Pow(xs[i+1]-xs[i],3)/3;
                }
                sum += ys[j]*(z-xs[j])+b[j]*Pow(z-xs[j],2)/2+c[j]*Pow(z-xs[j],3)/3;
                return sum;
	}

	public double quadterpDeriv(double z){
		int j = binsearch(z);
                double sum = b[j]+c[j]*(z-xs[j])*2;
                return sum;
	}
}
