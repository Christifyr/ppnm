using static System.Console;
using System.Collections.Generic;
class main{
        public static void Main(){
                var ind = In;
                string s;
		List<double> x = new List<double>();
		List<double> y = new List<double>();
		int i = 0;
                while((s=ind.ReadLine()) != null){
                        string sep = "\t";
			string[] split = s.Split(sep.ToCharArray());
			x.Add(double.Parse(split[0]));
			y.Add(double.Parse(split[1]));
			i +=1;
                }
                ind.Close();
		linIntrapol intra = new linIntrapol(x,y);
		quadIntrapol quad = new quadIntrapol(x,y);
		cubicIntrapol cub = new cubicIntrapol(x,y);
		for(double j = x[0]; j< x[i-1]; j += 0.01){
			WriteLine($"{j}	{intra.linterp(j)}	{intra.linterpIntegral(j)}	{quad.quadterp(j)}	{quad.quadterpIntegral(j)}	{quad.quadterpDeriv(j)} {cub.cubterp(j)}	{cub.cubterpIntegral(j)}	{cub.cubterpDeriv(j)}");	
		}
        }
}

