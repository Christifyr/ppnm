using System;
using static System.Math;
using System.Collections.Generic;
public class linIntrapol{
	List<double> xs;
	List<double> ys;
	List<double> p;
	List<double> b;
	List<double> c;

	public linIntrapol(List<double> x, List<double> y){
		this.xs = x;
		this.ys = y;
		p = new List<double>();
		for(int i = 0; i< xs.Count-1; i++){
			p.Add((ys[i+1]-ys[i])/(xs[i+1]-xs[i]));
		}
	}
	
	private int binsearch(double z){
		int i = 0;
                int j = xs.Count-1;
                while(j-i>1){
                        int m = (j+i)/2;
                        if(z-xs[m] > 0){ i = m;}
                        else{j=m;}
                }
		return i;}

	public double linterp(double z){
		int i = binsearch(z);
		return ys[i]+p[i]*(z-xs[i]);
	}
	
	public double linterpIntegral(double z){
		int j = binsearch(z);
		double sum = 0;
		for(int i = 0; i<j;i++){
			sum += ys[i]*(xs[i+1]-xs[i])+ p[i]*Pow(xs[i+1]-xs[i],2)/2;
		}
		sum += ys[j]*(z-xs[j]) + p[j]*Pow(z-xs[j],2)/2;
		return sum;
	}
}
