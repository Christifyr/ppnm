using System;
using static System.Math;
using System.Collections.Generic;
public class cubicIntrapol{
	List<double> xs;
	List<double> ys;
	List<double> b;
	List<double> c;
	List<double> d;
	
	public cubicIntrapol(List<double> x, List<double> y){
		this.xs = x;
		this.ys = y;
		int n = xs.Count;
		List<double> h = new List<double>();
		List<double> p = new List<double>();
		b = new List<double>();
		c = new List<double>();
		d = new List<double>();
		List<double> D = new List<double>();
		List<double> Q = new List<double>();
		List<double> B = new List<double>();
		for(int i=0; i<n-1;i++){
			h.Add(xs[i+1]-xs[i]);
			p.Add((ys[i+1]-ys[i])/(xs[i+1]-xs[i]));
			b.Add(0);
		}
		D.Add(2); Q.Add(1); B.Add(3*p[0]);
                for(int i=0; i<n-2;i++){
                        D.Add(h[i]/h[i+1]*2+2);
                        Q.Add(h[i]/h[i+1]);
                        B.Add(3*(p[i]+p[i+1]*h[i]/h[i+1]));
                }
		B.Add(3*p[n-2]); D.Add(2);
		for(int i=1; i<n; i++){
                        D[i]-=Q[i-1]/D[i-1];
                        B[i]-=B[i-1]/D[i-1];
                }
                b.Add(B[n-1]/D[n-1]);
                for(int i=n-2;i>=0;i--){
                        b[i]=(B[i]-Q[i]*b[i+1])/D[i];
                }
		for(int i=0; i<n-1;i++){
                        c.Add((-2*b[i]-b[i+1]+3*p[i])/h[i]);
                        d.Add((b[i]+b[i+1]-2*p[i])/Pow(h[i],2));
                }

	}
	
        private int binsearch(double z){
                int i = 0;
                int j = xs.Count-1;
                while(j-i>1){
                        int m = (j+i)/2;
                        if(z-xs[m] > 0){ i = m;}
                        else{j=m;}
                }
	        return i;
	}

	public double cubterp(double z){
		int i = binsearch(z);
		return ys[i]+b[i]*(z-xs[i])+c[i]*Pow(z-xs[i],2)+d[i]*Pow(z-xs[i],3);
	}

	public double cubterpIntegral(double z){
		int i = binsearch(z);
		double sum = 0;
		for(int j=0; j<i;j++){
			sum+= ys[j]*(xs[j+1]-xs[j])+b[j]*Pow(xs[j+1]-xs[j],2)/2+c[j]*Pow(xs[j+1]-xs[j],3)/3+d[j]*Pow(xs[j+1]-xs[j],4)/4;
		}
		sum+= ys[i]*(z-xs[i])+b[i]*Pow(z-xs[i],2)/2+c[i]*Pow(z-xs[i],3)/3+d[i]*Pow(z-xs[i],4)/4;
		return sum;
	}

	public double cubterpDeriv(double z){
                int i = binsearch(z);
  		double sum= b[i]+c[i]*Pow(z-xs[i],1)*2+d[i]*Pow(z-xs[i],2)*3;
                return sum;
        }

}
