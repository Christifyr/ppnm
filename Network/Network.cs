using System;
using static System.Math;
using static System.Console;
public class network{
	public int n;
	public vector p;
	public Func<double,double> f;
	
	public network(int n, Func<double,double> f){
		this.n=n;
		this.f=f;
		p = new vector(3*n);
	}

	public double feed(double x){
		double sum=0;
		for(int i=0; i<n;i++){
			double a = p[3*i+0];
			double b = p[3*i+1];
			double w = p[3*i+2];
			sum += w*f((x-a)/b);
		}
		return sum;
	}
	
	public double derivative(double x){
		double sum1 = feed(x);
		double h=1.0/64;
		double sum2 = feed(x+h);
		return (sum2-sum1)/h;
	}
	
	public double integral(double a,double b){
		int n = 100;
		double dx = (b-a)/n;
		double sum1 = 0;
		double sum2 = 0;
		for(int i=0;i<n;i++){
			sum1 += feed(a+i*dx)*dx;
			sum2 += feed(a+(i+1)*dx)*dx;
		}
		return (sum1+sum2)/2;
	}
		
	public void train(double[] xs,double[] ys){
		Func<vector,double> diviation = (z) =>{
			p=z;
			double sum = 0;
			for(int i=0;i<xs.Length;i++){
				sum+=Pow(feed(xs[i])-ys[i],2);
			}
			return sum/xs.Length;
		};
		vector v = p;
		vector min = quasinewton.qnewton(diviation,v,1e-6);
		p = min;
	}
}
