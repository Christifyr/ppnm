using System;
using static System.Math;
using static System.Console;
public class main{
	private static Func<double,double> activ = (x) => Cos(x)*Exp(-x*x);
	public static void Main(){
		int n = 30;
		double a = -PI;
		double b = 7*PI;
		double c = 0.0;
		double yc = 1.0;
		double dyc= -0.5;
		Func<vector,double,vector> myfun = (y,x) => new vector(y[1],1-y[0]+0.01*y[0]*y[0]);
		var oddann=new odenetwork(n,activ,yc,dyc,c,a,b);
		for(int i = 0; i<n;i++){
			oddann.p[3*i+0]= a+(b-a)*i/(oddann.n-1);
			oddann.p[3*i+1]= 1;
			oddann.p[3*i+2]= 1;
		}
		oddann.train(myfun);
		for(double x = a;x<b;x+=1.0/16){
			WriteLine($"{x}	{oddann.feed(x)}");
		}
	}
}
