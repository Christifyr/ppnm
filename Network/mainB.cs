using System;
using static System.Math;
using static System.Console;
public static class main{
	
	private static Func<double,double> activ = (x) => x*Exp(-x*x);
	private static Func<double,double> fit = (x) => Sin(x);
	public static void Main(string[] args){
		int n=5;
		var ann = new network(n,activ);
		int size = 25;
		double a = -1;
		double b = 1;
		double[] xs = new double[size];
		double[] ys = new double[size];
		for(int i = 0; i<size;i++){
			xs[i]=a+(b-a)*i/(size-1);
			ys[i]=fit(xs[i]);
			WriteLine($"{xs[i]}	{ys[i]}	{Cos(xs[i])}	{-Cos(xs[i])}");
		}
		for(int i = 0; i<n;i++){
			ann.p[3*i+0]= a+(b-a)*i/(ann.n-1);
			ann.p[3*i+1]= 1;
			ann.p[3*i+2]= 1;
		}
		Write("\n\n");
		ann.train(xs,ys);
		for(double z=a; z<=b;z+=1.0/64){
			WriteLine($"{z}	{ann.feed(z)}	{ann.derivative(z)}	{ann.integral(-PI/2,z)}");
		}
	}
}
