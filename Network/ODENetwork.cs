using System;
using static System.Math;
using static System.Console;
public class odenetwork{
        public int n;
        public vector p;
        public Func<double,double> f;
        public double yc;
        public double dyc;
        public double a;
        public double b;
        public double c;

        public odenetwork(int n, Func<double,double> f, double yc, double dyc,double c,double a, double b){
                this.n=n;
                this.f=f;
                this.yc=yc;
                this.dyc=dyc;
                this.a=a;
                this.b=b;
                this.c=c;
                p = new vector(3*n);
        }


	public double feed(double x){
		double sum=0;
		for(int i=0; i<n;i++){
			double a = p[3*i+0];
			double b = p[3*i+1];
			double w = p[3*i+2];
			sum += w*f((x-a)/b);
		}
		return sum;
	}

	public double derivative(double x){
                        double sum1 = feed(x);
                        double h=1.0/64;
                        double sum2 = feed(x+h);
                        return (sum2-sum1)/h;
                }
	
	public double integral(Func<vector,double,vector> phi,double a,double b){
		int n = 100;
		double dx = (b-a)/n;
		vector placeholder = new vector(2);
		vector placeholder2 = new vector(2);
		double fx = 0;
		double fx2 = 0;
		for(int i=0;i<n;i++){
			fx  += phi(new vector(f(a+i*dx),derivative(a+i*dx)),a+i*dx).norm()*dx;
			fx2 += phi(new vector(f(a+(i+1)*dx),derivative(a+(i+1)*dx)),a+(i+1)*dx).norm()*dx;
		}
		return (fx+fx2)/2;
	}
		
	public void train(Func<vector,double,vector> phi){
		Func<vector,double> loss = (z) =>{
			p=z;
			double result=integral(phi,a,b)+Abs(f(c)-yc)*(b-a)+Abs(derivative(c)-dyc)*(b-a);
			return result;
		};
		vector v = p;
		vector min = quasinewton.qnewton(loss,v,1e-6);
		p = min;
	}
}
