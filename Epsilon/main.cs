using static System.Math;
using static System.Console;
using static myapprox;
class main{
	static void Main(){
		int i=1; while(i+1>i) {i++;}
		Write("My maximum value is: {0}\n",i);
		Write("They are the same: {0}\n",myapprox.approx(i,int.MaxValue));
		i=1; while(i-1<i) {i--;}
		Write("My minimum value is {0}\n",i);
		Write("The difference is: {0}\n",i-int.MinValue);
		double x=1; while(1+x!=1){x/=2;} x*=2;
		float y=1F; while((float)(1F+y) != 1F){y/=2F;} y*=2F;
		Write("My eps for double is: {0}\n",x);
		Write("My eps for float is: {0}\n",y);
		Write("Checking that the one i got for double matches the lowest (my - eps) = {0}\n",x-Pow(2,-52));
		Write("My sum up is: {0}\n",sumFUp(int.MaxValue/2));
		Write("My sum down is: {0}\n",sumFDown(int.MaxValue/2));
		Write("There is a difference, because the float pointer is only 32 bit, or single precision, when we sum up the compiler determins that all the small numbers has no significance when compared to the value stored and throws it away.\n When summing down, next number comparable with the the one before, and it will therefore not optimize and throw values away.\n When doing the double precission or 64 bit pointer, we will therefore expect that the sums be the same");
		Write("The sum does converge aslong as the max is finite\n");
		Write("The integer sum up is: {0:f2}\n",sumDUp(int.MaxValue/3));
		Write("The integer summed down is: {0:f2}\n",sumDDown(int.MaxValue/3));
		Write("We can see that the assumption is correct\n");
	}

	static float sumFUp(int max){
		float f = 1F;
		for(int i=2;i<=max;i++){
			f+=1F/i;
		}
		return f;
	}

	static float sumFDown(int max){
		float f = 1F/max;
		for(int i=max-1; i>0;i--){
			f+=1F/i;
		}
		return f;
	}

	static double sumDUp(int max){
		double x = 1.0;
		for(int i=2;i<=max;i++){
			x+=1.0/i;
		}
		return x;
	}

	static double sumDDown(int max){
		double x = 1.0/max;
		for(int i=max-1;i>0;i--){
			x+=1.0/i;
		}
		return x;
	}

}
