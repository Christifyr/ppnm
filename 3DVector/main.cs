using static System.Console;
public class main{
	public static void Main(){
		vector3d v1 = new vector3d(1,2,3);
		vector3d v2 = new vector3d(1,1,1);
		Write("{0}+{1}={2}\n",v1,v2,v1+v2);
		Write("{0}-{1}={2}\n",v1,v2,v1-v2);
		Write("{0}-2={1}\n",v1,v1-2);
		Write("2-{0}={1}\n",v1,2-v1);
		Write("{0}*2={1}\n",v1,v1*2);
		Write("{0}/2={1}\n",v1,v1/2);
		Write("{0}dot{1}={2}\n",v1,v2,v1.dot(v2));
		Write("{0}cross{1}={2}\n",v1,v2,v1.cross(v2));
		Write("is {0}=={0} {1}\n",v1,v1.Equals(v1));
	}
}
