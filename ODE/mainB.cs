using System;
using static System.Math;
using static System.Console;
using System.Collections.Generic;
class main{
	public static void Main(){
		double R0 = 2.6; /* The typical number of new infected by one infectious individual */
		double Tr = 14; /*The recovery time is typically 2 weeks*/
		double Tc = Tr/R0; /*The typical time between contacts roughly */
		double N = 5811413.0/1000; /*The total population of Denmark in thousends*/
		Func<double,vector,vector> myFun = (t,y) => {
			return new vector(-y[1]*y[0]/(N*Tc),y[1]*y[0]/(N*Tc)-y[1]/Tr,y[1]/Tr);
		};		
		double a = 0;
                double b = 200;
                List<double> xs = new List<double>();
                List<vector> ys = new List<vector>();
		double I = 1;
		double S = N-I; /*The amount of people suceptible is the total polulation - the infectet */
		double R = 0;
                vector ya = new vector(S,I,R);
                ODE.ode45(myFun,a,b,ya,1,xs,ys,1e-5,1e-5);
                for(int i=0;i<xs.Count;i++){
                        WriteLine($"{xs[i]}     {ys[i][0]}      {ys[i][1]}	{ys[i][2]}");
                }
	
	}
}
