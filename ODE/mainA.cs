using System;
using static System.Math;
using static System.Console;
using System.Collections.Generic;
class main{
	public static Func<double,vector,vector> myfun(double eps){
		return (x,y) => new vector(y[1],1-y[0]+eps*y[0]*y[0]);
	}
	public static void Main(string[] args){
		double a = 0.0;
		double b = 7*PI;
                List<double> xs = new List<double>();
		List<vector> ys = new List<vector>();
		vector ya2 = new vector(1,-0.5);
		ODE.ode45(myfun(0.01),a,b,ya2,0.1,xs,ys,1e-3,1e-3);
                for(int i=0;i<xs.Count;i++){
                        WriteLine($"{xs[i]}     {ys[i][0]}      {ys[i][1]}");
                }
	}
}
