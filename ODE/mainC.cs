using System;
using static System.Math;
using static System.Console;
using System.Collections.Generic;
class main{
	public static void Main(){
		/*I feel like i need to explaim what is going on
		  In the first 2 are the x, y of the first planet, and the next two is the velocity, then follwes the x,y of the second and so on
		 */
		Func<double,vector,vector> myFun = (t,y) =>{
			double x1 = y[0], y1 = y[1], vx1 = y[2], vy1 = y[3], x2 = y[4],	y2 = y[5], vx2 = y[6], vy2 = y[7], x3 = y[8], y3 = y[9], vx3 = y[10], vy3 = y[11];
			vector yt = new vector(12);
			yt[0] = vx1;
			yt[1] = vy1;
			yt[2] = -(x1-x2)/Pow(Sqrt(Pow((x1-x2),2)+Pow((y1-y2),2)),3)-(x1-x3)/Pow(Sqrt(Pow((x1-x3),2)+Pow((y1-y3),2)),3);
			yt[3] = -(y1-y2)/Pow(Sqrt(Pow((x1-x2),2)+Pow((y1-y2),2)),3)-(y1-y3)/Pow(Sqrt(Pow((x1-x3),2)+Pow((y1-y3),2)),3);
			yt[4] = vx2;
                        yt[5] = vy2;
                        yt[6] = -(x2-x1)/Pow(Sqrt(Pow((x2-x1),2)+Pow((y2-y1),2)),3)-(x2-x3)/Pow(Sqrt(Pow((x2-x3),2)+Pow((y2-y3),2)),3);
                        yt[7] = -(y2-y1)/Pow(Sqrt(Pow((x2-x1),2)+Pow((y2-y1),2)),3)-(y2-y3)/Pow(Sqrt(Pow((x2-x3),2)+Pow((y2-y3),2)),3);
			yt[8] = vx3;
                        yt[9] = vy3;
                        yt[10] = -(x3-x2)/Pow(Sqrt(Pow((x3-x2),2)+Pow((y3-y2),2)),3)-(x3-x1)/Pow(Sqrt(Pow((x3-x1),2)+Pow((y3-y1),2)),3);
                        yt[11] = -(y3-y2)/Pow(Sqrt(Pow((x3-x2),2)+Pow((y3-y2),2)),3)-(y3-y1)/Pow(Sqrt(Pow((x3-x1),2)+Pow((y3-y1),2)),3);
			return yt;
		};
		double x10 = 0;
		double y10 = 0;
		double vx10 = -0.93240737;
		double vy10 = -0.86473146;
		double x20 = -0.97000436;
                double y20 = 0.24308753;
                double vx20 = 0.4662036850;
                double vy20 = 0.4323657300;
		double x30 = -x20;
                double y30 = -y20;
                double vx30 = vx20;
                double vy30 = vy20;
		double[] temp = new double[12]{x10,y10,vx10,vy10,x20,y20,vx20,vy20,x30,y30,vx30,vy30};
		vector y0 = new vector(temp);
		double a = 0;
		double b = 6.3259;
		List<double> xs = new List<double>();
                List<vector> ys = new List<vector>();
		ODE.ode45(myFun,a,b,y0,0.001,xs,ys,1e-4,1e-4);
		for(int i = 0; i<xs.Count;i++){
			WriteLine($"{ys[i][0]}	{ys[i][1]}	{ys[i][4]}	{ys[i][5]}	{ys[i][8]}	{ys[i][9]}");		
		}
	}
}
