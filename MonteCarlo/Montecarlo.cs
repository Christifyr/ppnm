using System;
using static System.Math;
using static System.Console;
public static class MC{
	public static Func<Func<vector,double>,vector,vector,int,vector> plainmc = (f,a,b,N) => {
		int dim = a.size;
		var rnd = new Random();
		double V = 1; for(int i=0;i<dim;i++) V*=(b[i]-a[i]);
		double sum=0;
		double sum2=0; 
		double fx;
		vector x = new vector(dim);
		for(int i=0;i<N;i++){
			for(int j =0;j<dim;j++) x[j]=a[j]+rnd.NextDouble()*(b[j]-a[j]);
			fx=f(x);
			sum+=fx;
			sum2+=fx*fx;
		}
		double avr = sum/N;
		double var = sum2/N-avr*avr;
		double result = avr*V;
		double err = Sqrt(var/N)*V;
		return new vector(result,err);
	};
	
	public static Func<Func<vector,double>,vector,vector,int,double,vector> rssampling = (f,a,b,N,acc) => {
                vector result = rssamp(f,a,b,N,acc);
		return result;
        };

	public static Func<Func<vector,double>,vector,vector,int,double,vector> rssamp = (f,a,b,N,acc) => {
		vector sample = plainmc(f,a,b,N);
		if(sample[1] <= acc){
			return sample;
		}
		int dim = a.size;
		vector[] samples1 = new vector[dim];
		vector[] samples2 = new vector[dim];
		double hivar = 0;
		int hi =0;
		double factor =0;
		for(int i=0; i<dim; i++){
			factor = 0.5*(b[i]-a[i]);
			vector placeholder = b.copy();
			placeholder[i] -= factor;
			samples1[i] = plainmc(f,a,placeholder,N/dim/2);
			samples2[i] = plainmc(f,placeholder,b,N/dim/2);
			double current = (Pow(samples1[i][1],2)+Pow(samples2[i][1],2))/N/dim/4;
			if(current>hivar){
				hivar = current;
				hi = i;
			}
		}
		factor = 0.5*(b[hi]-a[hi]);
		vector placeholder2 = b.copy();
		vector placeholder3 = a.copy();
		placeholder2[hi] -= factor;
		placeholder3[hi] += factor;
		vector result = rssamp(f,a,placeholder2,N/2,acc*Sqrt(N/2))+rssamp(f,placeholder3,b,N/2,acc*Sqrt(N/2));
		return new vector(result[0],Pow(result[1],2)/N/4);
	};
}
