using System;
using static System.Math;
using static System.Console;
class main{
	public static void Main(){
		Func<vector,double> myfun2 = (y) => {return y[0]*y[0]+4*y[1];};
		vector a2 = new vector(11,7);
		vector b2 = new vector(14,10);
		vector x2 = MC.rssampling(myfun2,a2,b2,1000000,1e-6);
		WriteLine($"{x2[0]}");
	}
}
