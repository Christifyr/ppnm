using System;
using static System.Math;
using static System.Console;
class main{
	public static void Main(){
		Func<vector,double> myfun2 = (y) => {return y[0]*y[0]+4*y[1];};
		vector a2 = new vector(11,7);
		vector b2 = new vector(14,10);
		int n = 1;
		for(int i=1; i<5;i++){
			n*=100;
			vector x2 = MC.plainmc(myfun2,a2,b2,n);
			WriteLine($"The error estimate:{x2[1]} O(1/sqrt(N)): {1/Sqrt(n)}");}
	}
}
