using System;
using static System.Math;
using static System.Console;
class main{
	public static void Main(){
		Func<vector,double> myfun1 = (y) => {return Pow(1-Cos(y[0])*Cos(y[1])*Cos(y[2]),-1)/PI/PI/PI;};
		Func<vector,double> myfun2 = (y) => {return y[0]*y[0]+4*y[1];};
		vector a1 = new vector(0,0,0);
		vector b1 = new vector(PI,PI,PI);
		vector x1 = MC.plainmc(myfun1,a1,b1,100000);
		WriteLine($"{x1[0]}");
		vector a2 = new vector(11,7);
		vector b2 = new vector(14,10);
		vector x2 = MC.plainmc(myfun2,a2,b2,100000);
		WriteLine($"{x2[0]}");
	}
}
