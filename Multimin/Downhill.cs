using System;
using static System.Math;
using static System.Console;
public class downhill{
	private int dim;
	public downhill(){
	}
	void reflection(double[] hi, double[] cen, double[] refl){
		for(int i=0;i<dim;i++) refl[i]=2*cen[i]-hi[i];
	}
	void expansion(double[] hi, double[] cen, double[] exp){
		for(int i=0;i<dim;i++) exp[i]=3*cen[i]-2*hi[i];
	}
	void contraction(double[] hi, double[] cen, double[] con){
		for(int i=0;i<dim;i++) con[i]=1.0/2*cen[i]+1.0/2*hi[i];
	}
	void reduction(double[][] simplex, int lo){
		for(int i=0;i<dim;i++) if(i!=lo) for(int j=0;j<dim;j++){
			simplex[i][j]=1.0/2*(simplex[i][j]+simplex[lo][j]);}
	}
	double distance(double[] a, double[] b){
		double s = 0;
		for(int i=0;i<dim;i++) s+=Abs(b[i]-a[i]);
		return s;
	}
	double size(double[][] simplex){
		double s = 0;
		for(int i=0;i<dim;i++){
			double dist = distance(simplex[0],simplex[i]);
			if(dist>s) s=dist;		
		}
		return s;
	}

	void update(double[][] simplex, double[] fvals,int hi, int lo, double[] cen){
		hi=0; lo=0; 
		double highest=fvals[hi]; double lowest=fvals[lo];
		for(int i=0; i<dim; i++){
			double current = fvals[i];
			if(current>highest){highest=current;hi=i;}
			if(current<lowest){lowest=current;lo=i;}}
		for(int j=0; j<dim; j++){
			double s = 0;
			for(int i=0; i<dim+1; i++){
				if(i!=hi){
					s+=simplex[i][j];
				}
				cen[j]=s/dim;
			}
		}
	}
	
	void initiate(Func<vector,double> func,double[][] simplex, double[] fvals, int hi, int lo, double[] cen){
		for(int i=0;i<dim+1;i++) fvals[i]=func(new vector(simplex[i]));
		update(simplex,fvals,hi,lo,cen);
	}
	
	public double[] downsimp(Func<vector,double> func,double[] x,double sizegoal){
		this.dim=x.Length;
		double[][] simplex =new double[dim+1][];
		simplex[dim] = x;
		double[] fvals = new double[dim+1];
		for(int i=0;i<dim;i++){
			x[i]+=sizegoal;
			simplex[i] =(double[])x.Clone();
			fvals[i] = func(new vector(simplex[i]));
			x[i]-=sizegoal;			
		}
		int hi=0 , lo=0, k=0; 
		double[] cen=new double[dim] ,  p1= new double[dim] , p2=new double[dim];
		initiate(func,simplex,fvals,hi,lo,cen);
		while(size(simplex)>sizegoal){
			update(simplex,fvals,hi,lo,cen);
			reflection(simplex[hi],cen,p1); double re = func(new vector(p1));
			if(re<fvals[lo]){
				expansion(simplex[hi],cen,p2); double ex = func(new vector(p2));
				if(ex<re){
					for(int i=0;i<dim;i++)simplex[hi][i]=p2[i];fvals[hi]=ex;
				}
				else{
					for(int i=0;i<dim;i++)simplex[hi][i]=p1[i];fvals[hi]=re;
				}
			}
			if(re<fvals[hi]){
				for(int i=0;i<dim;i++)simplex[hi][i]=p1[i];fvals[hi]=re;
			}
			else{
				contraction(simplex[hi],cen,p2); double con = func(new vector(p2));
				if(con<fvals[hi]){
                                        for(int i=0;i<dim;i++)simplex[hi][i]=p2[i];fvals[hi]=con;
                                }
                                else{
                                        reduction(simplex,lo);
					initiate(func,simplex,fvals,hi,lo,cen);
                                }

			}
		k++;}
		x = simplex[lo];
		return x;
	}
}
