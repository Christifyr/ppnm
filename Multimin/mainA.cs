using System;
using static System.Math;
using static System.Console;
class main{
	public static void Main(){
		Func<vector,double> f1 = (y) =>{
			double result = Pow(1-y[0],2)+100*Pow(y[1]-y[0]*y[0],2);
			return result;
		};
		Func<vector,double> f2 = (y) =>{ return Pow(y[0]*y[0]+y[1]-11.0,2)+Pow(y[0]+y[1]*y[1]-7.0,2);};
		double eps = 1e-10;
		vector x1 = new vector(3,3);
		vector min = quasinewton.qnewton(f1,x1,eps);
		WriteLine("Rosenbrock's:");
		WriteLine("The found values that gives the minimum is:");
		WriteLine($"x={min[0]} y={min[1]}");
		WriteLine($"Which gives the value {f1(min)}");
		min = quasinewton.qnewton(f2,x1,eps);
		WriteLine("Himmelblau's:");
                WriteLine("The found values that gives the minimum is:");
                WriteLine($"x={min[0]} y={min[1]}");
                WriteLine($"Which gives the value {f2(min)}");

	}
}
