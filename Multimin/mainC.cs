using System;
using static System.Math;
using static System.Console;
class main{
	public static void Main(){
		double[] energy = new double[]{101,103,105,107,109,111,113,115,117,119,121,123,125,127,129,131,133,135,137,139,141,143,145,147,149,151,153,155,157,159};
                double[] sigma = new double[]{-0.25,-0.30,-0.15,-1.71,0.81,0.65,-0.91,0.91,0.96,-2.52,-1.01,2.01,4.83,4.58,1.26,1.01,-1.26,0.45,0.15,-0.91,-0.81,-1.41,1.36,0.50,-0.45,1.61,-2.21,-1.86,1.76,-0.50};
                double[] err = new double[]{2.0,2.0,1.9,1.9,1.9,1.9,1.9,1.9,1.6,1.6,1.6,1.6,1.6,1.6,1.3,1.3,1.3,1.3,1.3,1.3,1.1,1.1,1.1,1.1,1.1,1.1,1.1,0.9,0.9,0.9};
                Func<vector,double> chi2 = (y) => {
                        double sum=0;
                        for(int i = 0; i<energy.Length; i++){
                                sum += Pow(y[2]/(Pow(energy[i]-y[0],2)+y[1]*y[1]/4)-sigma[i],2)/err[i]/err[i];
                        }
                        return sum;
                };
                double eps = 1e-10;
                double[] x = new double[]{126.0,3.0,6.0};
		downhill down= new downhill();
                double[] min = down.downsimp(chi2,x,eps);
                WriteLine("The mass found in natural units is:");
		WriteLine($"m={min[0]} Gev");
		}
}
