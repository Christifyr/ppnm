using System;
using static System.Math;
using static System.Console;
public static class quasinewton{
	
	public static vector gradient(Func<vector,double> f,vector x, double eps){
		double fx = f(x);
		int n = x.size;
		vector g = new vector(n);
		for(int i = 0; i<n; i++){
			x[i]+=eps;
			g[i] = (f(x)-fx)/eps;	
			x[i]-=eps;
		}
		return g;
	}
	
	public static Func<Func<vector,double>,vector,double,vector> qnewton = (f,x0,eps) => {
		double fx = f(x0);
		vector gx = gradient(f,x0,eps);
		matrix B = new matrix(x0.size,x0.size);
		B.setid();
		int n=0;
		while(n<999){
			n++;
			vector Dx =-B*gx;
			if(gx.norm()<eps) break;
			double lambda = 1.0;
			while(f(x0+lambda*Dx)>fx){
				if(lambda<eps){
					B.setid();
					break;
				}
				lambda/=2.0;
			}
			vector s = lambda*Dx;
			vector gz = gradient(f,x0+s,eps);
			vector y = gz-gx;
			vector u = s-B*y;
			if(Abs(u%y)>1e-6){
				B.update(u,u,1.0/(u%y));
			}
			x0 = x0+s;
			gx = gz;
			fx = f(x0);
		}
		return x0;
	};

	
}
