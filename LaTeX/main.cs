using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
class main{
	
	public static Func<double,vector,vector> legendre(double n){
		return (x,y) => new vector(y[1],(-n*(n+1)*y[0]+2*x*y[1])/(1-Pow(x,2))); 
	}

	public static void Main(string[] args){
		double d = double.Parse(args[0]);
		double a = 0;
	       	double b = 1;
		vector ya = new vector(double.Parse(args[1]),double.Parse(args[2]));
		List<double> xs = new List<double>();
		List<vector> ys = new List<vector>();
		vector yb = ode.rk23(legendre(d),a,ya,b,xs,ys);
		for (int i = xs.Count-1; i>0;i--){
			if(d==0 || d==2 || d==4){
				double k = 0-xs[i];
				WriteLine($"{k}	{ys[i][0]}");
			}
			else{
				double k = 0-xs[i];
				double q = 0-ys[i][0];
				WriteLine($"{k}	{q}");
			}	
		}
		for(int i = 0; i<xs.Count;i++){
			WriteLine($"{xs[i]}	{ys[i][0]}");
		}
	}

}
